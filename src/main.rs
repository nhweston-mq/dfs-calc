use std::env;
use std::error::Error;

use crate::csv_selectable::SelectableTable;
use crate::knapsack::Knapsack;

mod csv_selectable;
mod csv_slot;
mod frame;
mod knapsack;
mod selectable;
mod solution;
mod u8set;

fn load (
    sel_csv_path: &String,
    slot_csv_path: &String,
    budget: &String
) -> Result<(Knapsack, SelectableTable), Box<dyn Error>> {
    let slot_table = csv_slot::load_slots(slot_csv_path.as_str())?;
    let selectable_table = csv_selectable::load_selectables (
        sel_csv_path.as_str(),
        &slot_table
    ) ?;
    let budget: u64 = budget.parse()?;
    let selectables = Vec::clone(&selectable_table.sels_by_idx);
    let slots = Vec::clone(&slot_table.slots);
    let knapsack = Knapsack {sels: selectables, slots, budget};
    Ok((knapsack, selectable_table))
}

fn run(knapsack: Knapsack, selectable_table: SelectableTable) {
//    println!("{:#?}", selectable_table.sels_by_idx);
    match knapsack.find_optimal_solution() {
        Some(solution) => {
            let mut cost_total = 0;
            let mut value_total = 0;
            println!("{0: <24} {1: >12} {2: >12}", "Label", "Cost", "Value");
            for selectable_idx in solution.selected {
                let selectable = &selectable_table.sels_by_idx[selectable_idx];
                let label = &selectable.label;
                let cost = &selectable.cost;
                let value = &selectable.value;
                cost_total += *cost;
                value_total += *value;
                println!("{0: <24} {1: >12} {2: >12}", label, cost, value);
            }
            println!("{0: <24} {1: >12} {2: >12}", "TOTAL", cost_total, value_total);
        }
        None => {
            println!("No solution!");
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    match (args.get(1), args.get(2), args.get(3)) {
        (Some(selectable_csv_path), Some(slot_csv_path), Some(budget_as_string)) => {
            match load(selectable_csv_path, slot_csv_path, budget_as_string) {
                Ok((knapsack, selectable_table)) => run(knapsack, selectable_table),
                Err(e) => eprintln!("{}", e)
            }
        }
        _ => panic!("Missing arguments")
    }
}
