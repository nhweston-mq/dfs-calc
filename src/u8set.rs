#[derive(Copy, Clone, Debug)]
pub struct U8Set {
    data: [u64; 4],
    size: u8
}

const NUM_BITS: u8 = 64;
const NUM_WORDS: usize = 4;

impl U8Set {

    pub fn new() -> Self {
        U8Set {data: [0; NUM_WORDS], size: 0}
    }

    pub fn from_values(values: &[u8]) -> Self {
        let mut data = [0; NUM_WORDS];
        for value in values {
            let (iarr, ibit) = U8Set::locate(*value);
            data[iarr] = data[iarr] + (1 << ibit);
        }
        U8Set {data, size: U8Set::compute_size(&data)}
    }

    pub fn fill_range(min: u8, max: u8) -> Self {
        let loc = U8Set::locate(min);
        let mut iarr = loc.0;
        let mut bit = 1 << loc.1;
        let mut data = [0; NUM_WORDS];
        for _ in min..max {
            data[iarr] |= bit;
            bit <<= 1;
            if bit == 0 {
                iarr += 1;
                bit = 1;
            }
        }
        U8Set {data, size: max-min}
    }

    fn locate(value: u8) -> (usize, u64) {
        ((value / NUM_BITS) as usize, (value % NUM_BITS) as u64)
    }

    fn compute_size(data: &[u64; NUM_WORDS]) -> u8 {
        let mut result: u8 = 0;
        for i in 0..NUM_WORDS {
            let mut temp = data[i];
            while temp > 0 {
                if temp % 2 == 1 {
                    result = result + 1;
                }
                temp >>= 1;
            }
        }
        result
    }

    pub fn is_empty(&self) -> bool {
        for i in 0..NUM_WORDS {
            if self.data[i] != 0 {
                return false;
            }
        }
        return true;
    }

    pub fn size(&self) -> u8 {
        return self.size;
    }

    pub fn intersection(&self, other: &Self) -> Self {
        let mut data = [0; NUM_WORDS];
        for i in 0..NUM_WORDS {
            data[i] = self.data[i] | other.data[i];
        }
        U8Set {data, size: U8Set::compute_size(&data)}
    }

    pub fn insert(&mut self, value: u8) {
        let (iarr, ibit) = U8Set::locate(value);
        let prev = self.data[iarr];
        self.data[iarr] |= 1 << ibit;
        if self.data[iarr] != prev {
            self.size += 1;
        }
    }

    pub fn remove(&mut self, value: u8) {
        let (iarr, ibit) = U8Set::locate(value);
        let prev = self.data[iarr];
        self.data[iarr] &= !(1 << ibit);
        if self.data[iarr] != prev {
            self.size -= 1;
        }
    }

    pub fn values(&self) -> Vec<u8> {
        let mut result = Vec::new();
        let mut value = 0;
        let mut iarr = 0;
        let mut bit = 0;
        let mut temp = self.data[iarr];
        loop {
            if temp % 2 == 1 {
                result.push(value);
            }
            if bit == NUM_BITS - 1 {
                if iarr == NUM_WORDS - 1 {
                    break result;
                }
                else {
                    value += 1;
                    iarr += 1;
                    bit = 0;
                    temp = self.data[iarr];
                }
            }
            else {
                value += 1;
                bit += 1;
                temp >>= 1;
            }
        }
    }

}
