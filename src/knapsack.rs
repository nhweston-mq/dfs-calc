use crate::frame::Frame;
use crate::selectable::Selectable;
use crate::solution::Solution;
use crate::u8set::U8Set;

#[derive(Debug)]
pub struct Knapsack {
    pub sels: Vec<Selectable>,
    pub slots: Vec<U8Set>,
    pub budget: u64
}

impl Knapsack {

    pub fn find_optimal_solution(&self) -> Option<Solution> {
        let mut sel_idxs = Vec::new();
        for i in 0..self.sels.len() {
            sel_idxs.push(i);
        }
        sel_idxs.sort_by(|x, y| self.sels[*y].cost.cmp(&self.sels[*x].cost));
        let head = Frame::initial(self);
        head.find_optimal_selection()
    }

    pub fn can_fit(&self, selected: &[usize]) -> bool {
        self.can_fit_aux (
            selected,
            U8Set::fill_range(0, selected.len() as u8),
            U8Set::fill_range(0, self.slots.len() as u8)
        )
    }

    fn can_fit_aux (
        &self,
        selected: &[usize],
        selected_idxs: U8Set,
        slot_idxs: U8Set,
    ) -> bool {
        if selected_idxs.is_empty() {return true;}
        if slot_idxs.is_empty() {return false;}
        // Find selectable with fewest possible slots
        let mut min_idx: u8 = 0xFF;
        let mut min_slots: U8Set = U8Set::new();
        let mut min_size = 0xFF;
        for i in selected_idxs.values() {
            let sel = &self.sels[selected[i as usize]];
            let mut cands = U8Set::new();
            for j in slot_idxs.values() {
                let slot = self.slots[j as usize];
                if !slot.intersection(&sel.categories).is_empty() {
                    cands.insert(j as u8);
                }
            }
            let size = cands.size();
            if size == 0 {return false;}
            else if size < min_size {
                min_idx = i;
                min_slots = cands;
                min_size = size;
            }
        }
        let mut selected_idxs_next = selected_idxs;
        selected_idxs_next.remove(min_idx);
        for slot_idx in min_slots.values() {
            let mut slot_idxs_next = slot_idxs;
            slot_idxs_next.remove(slot_idx);
            if self.can_fit_aux (selected, selected_idxs_next, slot_idxs_next) {
                return true;
            }
        }
        false
    }

}
