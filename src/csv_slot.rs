use std::collections::BTreeMap;
use std::error::Error;
use std::path::Path;

use std::fs;

use crate::u8set::U8Set;

pub struct SlotTable {
    pub slots: Vec<U8Set>,
    pub cats_by_idx: Vec<String>,
    pub idxs_by_cat: BTreeMap<String, u8>
}

pub fn load_slots(file_path: &str) -> Result<SlotTable, Box<dyn Error>> {
    let file = fs::read_to_string(Path::new(file_path))?;
    let rows = file.split_whitespace();
    let mut slot_table = SlotTable {
        slots: Vec::new(),
        cats_by_idx: Vec::new(),
        idxs_by_cat: BTreeMap::new()
    };
    for row in rows {
        parse_row(row.to_string(), &mut slot_table);
    }
    Ok(slot_table)
}

fn parse_row (
    row: String,
    slot_table: &mut SlotTable
) {
    let mut slot = U8Set::new();
    for cat_str in row.split(',') {
        let cat = cat_str.to_string();
        match slot_table.idxs_by_cat.get(&cat) {
            Some(idx) => slot.insert(*idx),
            None => {
                let idx = slot_table.cats_by_idx.len() as u8;
                slot_table.cats_by_idx.push(cat.to_string());
                slot_table.idxs_by_cat.insert(cat, idx);
                slot.insert(idx);
            }
        }
    }
    slot_table.slots.push(slot);
}
