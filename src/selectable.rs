use crate::u8set::U8Set;

#[derive(Clone, Debug)]
pub struct Selectable {
    pub label: String,
    pub cost: u64,
    pub value: i64,
    pub categories: U8Set
}
