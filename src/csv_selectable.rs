use std::collections::BTreeMap;
use std::error::Error;
use std::path::Path;

use serde::Deserialize;

use crate::csv_slot::SlotTable;
use crate::selectable::Selectable;
use crate::u8set::U8Set;

#[derive(Debug)]
pub struct SelectableTable {
    pub sels_by_idx: Vec<Selectable>,
    pub idxs_by_label: BTreeMap<String, usize>
}

#[derive(Deserialize)]
struct CSVRow {
    label: String,
    cost: u64,
    value: i64,
    categories: String
}

pub fn load_selectables (
    file_path: &str,
    slot_table: &SlotTable
) -> Result<SelectableTable, Box<dyn Error>> {
    let mut reader = csv::Reader::from_path(Path::new(file_path))?;
    let mut sels_by_idx = Vec::new();
    let mut idxs_by_label = BTreeMap::new();
    for result in reader.deserialize() {
        let row: CSVRow = result?;
        let selectable = parse_row(&row, slot_table);
        idxs_by_label.insert(row.label, idxs_by_label.len());
        sels_by_idx.push(selectable);
    }
    Ok(SelectableTable {sels_by_idx, idxs_by_label})
}

fn parse_row (
    row: &CSVRow,
    slot_table: &SlotTable
) -> Selectable {
    let mut categories = Vec::new();
    for cat in row.categories.split(',') {
        match slot_table.idxs_by_cat.get(cat) {
            Some(cat_idx) => categories.push(*cat_idx),
            None => ()
        }
    }
    Selectable {
        label: row.label.clone(),
        cost: row.cost,
        value: row.value,
        categories: U8Set::from_values(categories.as_slice())
    }
}
