use crate::knapsack::Knapsack;
use std::collections::VecDeque;
use crate::solution::Solution;

#[derive(Debug)]
pub struct Frame<'a> {
    knapsack: &'a Knapsack,
    sel_q: VecDeque<usize>,
    selected: Vec<usize>,
    budget: u64,
    value: i64
}

impl<'a> Frame<'a> {

    pub fn initial(knapsack: &'a Knapsack) -> Self {
        let mut sel_idxs = Vec::new();
        for i in 0..knapsack.sels.len() {
            sel_idxs.push(i);
        }
        sel_idxs.sort_by(|x, y| knapsack.sels[*y].value.cmp(&knapsack.sels[*x].value));
//        println!("{:#?}", sel_idxs);
        Frame {
            knapsack: &knapsack,
            sel_q: VecDeque::from(sel_idxs),
            selected: vec![],
            budget: knapsack.budget,
            value: 0
        }
    }

    fn max_prospective_value(&self) -> i64 {
        let mut result = 0;
        for i in 0 .. self.knapsack.slots.len() - self.selected.len() {
            match self.sel_q.get(i) {
                Some(s) => result += self.knapsack.sels[*s].value,
                None => {return 0;}
            }
        }
        self.value + result
    }

    fn is_selection_valid(&self, sel_idx: usize) -> bool {
        let sel = &self.knapsack.sels[sel_idx];
        sel.cost <= self.budget && self.knapsack.can_fit(self.selected.as_slice())
    }

    fn create_subframe(&self, sel_q_idx: usize) -> Frame {
        let sel_idx = match self.sel_q.get(sel_q_idx) {
            Some (sel_idx) => *sel_idx,
            None => panic!()
        };
        let selectable = &self.knapsack.sels[sel_idx];
        let mut sel_q_next = self.sel_q.clone();
        sel_q_next = sel_q_next.split_off(sel_q_idx + 1);
        let mut selected_next = self.selected.clone();
        selected_next.push(sel_idx);
        Frame {
            knapsack: self.knapsack,
            sel_q: sel_q_next,
            selected: selected_next,
            budget: self.budget - selectable.cost,
            value: self.value + selectable.value
        }
    }

    pub fn find_optimal_selection(&self) -> Option<Solution> {
        println!("{:?}", self.selected);
        if self.selected.len() == self.knapsack.slots.len() {
            return Some(Solution {
                selected: self.selected.clone(),
                value: self.value
            });
        }
        if self.sel_q.is_empty() {
            return None;
        }
        let mut sel_q_idx = 0;
        let mut result: Option<Solution> = None;
        loop {
            let sel_idx = self.sel_q[sel_q_idx];
            if self.is_selection_valid(sel_idx) {
                let next = self.create_subframe(sel_q_idx);
                match result {
                    Some(soln) => {
                        if next.max_prospective_value() < soln.value {
                            break Some(soln);
                        }
                        else {
                            result = match next.find_optimal_selection() {
                                Some(soln_next) => {
                                    if soln_next.value > soln.value {Some(soln_next)}
                                    else {Some(soln)}
                                },
                                None => Some(soln)
                            }
                        }
                    },
                    None => {
                        result = next.find_optimal_selection();
                    }
                }
            }
            sel_q_idx += 1;
            if sel_q_idx >= self.sel_q.len() {
                break result
            }
        }
    }

}